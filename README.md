`OpenStreetMap` scripts in Python.

# Prerequisites
- **Geopandas:** `pip install geopandas`
- **Pysal:** `pip install pysal`. Used to apply the `quantile` scheme to the plot and cluster the towns by population.
- **Algiers map extracted from OSM:** [Algiers Map (GeoJSON IMPOSM)](https://mapzen.com/data/metro-extracts/metro/algiers_algeria_expanded/)

# Other links to import OSM data
- **OpenStreetMap export:** [OSM export](https://www.openstreetmap.org/export)
- **Blog post tutorial:** [tutorial](http://michelleful.github.io/code-blog/2015/04/27/osm-data/)
- **Query OSM via Overpass:** [geopandas-osm](https://github.com/jwass/geopandas_osm)
